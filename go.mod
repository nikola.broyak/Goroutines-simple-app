module producer_consumer/producer_consumer

replace producer_consumer/consumer => ./consumer

replace producer_consumer/producer => ./producer

go 1.18

require (
	producer_consumer/consumer v0.0.0-00010101000000-000000000000
	producer_consumer/producer v0.0.0-00010101000000-000000000000
)
