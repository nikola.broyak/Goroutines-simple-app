package producer

import (
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

type Producer struct{}

func (p Producer) Run(
	ch chan string,
	wg *sync.WaitGroup,
	IDs []string,
) {
	p.createMessage(ch, IDs)
	wg.Done()
}

func (p Producer) createMessage(ch chan<- string, IDs []string) { // send-only channel
	exit := make(chan os.Signal, 1) // make channel for receiving signals
	idslen := int32(len(IDs))
	signal.Notify(exit, os.Interrupt, syscall.SIGTERM) // send signals to exit channel
	for i := 0; ; i++ {
		select {
		case <-exit:
			fmt.Println("Received interrupt, shutting down...")
			fmt.Println("Producer exit.")
			close(ch)
			return
		default:
			randnum := rand.Int31n(idslen)
			msg := IDs[randnum]
			fmt.Printf("\tProducing message #%v: %v\n", i, msg)
			ch <- msg
			time.Sleep(10 * time.Millisecond)
		}
	}
}
