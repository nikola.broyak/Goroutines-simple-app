package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"producer_consumer/consumer"
	"producer_consumer/producer"
	"strconv"
	"strings"
	"sync"
)

var wg = sync.WaitGroup{}

type Worker interface {
	Run(
		chan string,
		*sync.WaitGroup,
		[]string,
	)
}

func main() {
	pwd, _ := os.Getwd()
	if len(os.Args) == 1 {
		log.Fatal("No ID file specified")
	} else {
		filepath := path.Join(pwd, os.Args[1])
		IDs := processArgFile(filepath)
		runOneProdAndOneConsumer(IDs)

	}
}

func runOneProdAndOneConsumer(IDs []string) {
	producer := producer.Producer{}
	consumer := consumer.Consumer{}
	if typeSwitch(producer) && typeSwitch(consumer) {
		ch := make(chan string, 5) // buffered channel of size 5
		wg.Add(2)
		go producer.Run(ch, &wg, IDs) // Run Worker interface
		go consumer.Run(ch, &wg, IDs) // Run Worker interface
		wg.Wait()
		fmt.Println("Program gracefully shut down.")
		return
	}
	fmt.Println("Incorrect implementation of Worker interface")
}

func typeSwitch(i interface{}) bool {
	switch i.(type) {
	case Worker:
		return true
	default:
		return false
	}
}

func populateIDsSlice(file *os.File, linecount int) []string {
	reader := bufio.NewReader(file)
	IDs := make([]string, 0, linecount)
	for {
		line, err := reader.ReadBytes('\n')
		if err == nil {
			IDs = append(IDs, string(line))
			continue
		}
		break
	}
	return IDs
}

func getLineCount(filepath string) int {
	output, _ := exec.Command("wc", "-l", filepath).Output()
	stringoutput := strings.Split(string(output), " ")[0]
	linecount, _ := strconv.Atoi(stringoutput)
	return linecount
}

func processArgFile(filepath string) []string {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal("Incorrect file name")
	}
	linecount := getLineCount(filepath)
	IDs := populateIDsSlice(file, linecount)
	if len(IDs) == 0 {
		log.Fatal("Empty IDs file, aborting")
	}
	return IDs
}
