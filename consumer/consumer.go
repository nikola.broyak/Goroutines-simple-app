package consumer

import (
	"fmt"
	"sync"
	"time"
)

type Consumer struct{}

func (c Consumer) Run(
	ch chan string,
	wg *sync.WaitGroup,
	IDs []string,
) {
	c.consumeMessage(ch)
	wg.Done()
}

func (c Consumer) consumeMessage(ch <-chan string) { // read-only channel
	for i := 0; ; i++ {
		msg, ok := <-ch
		if !ok {
			fmt.Println("Consumer exit.")
			return
		}
		fmt.Printf("\t\tConsuming message #%v: %v\n", i, msg)
		time.Sleep(500 * time.Millisecond)

	}
}
